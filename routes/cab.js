var express = require('express');
var router = express.Router();
var Request = require('../models/request');
var Cab = require('../models/cab')

router.post('/create_cab', async function(req, res, next) {
  try{
    var req_body = req.body;
    var cab_detail = new Cab({
      cab_name: req_body.cab_name,
      cab_type: req_body.cab_type,
      location: [Number(req_body.latitude), Number(req_body.longitude)],
      driver_detail: {
        name: req_body.name,
        phone: req_body.phone,
        email: req_body.email,
      },
      vehicle_detail: {
        name: req_body.name,
        type: req_body.type,
        plate_no: req_body.plate_no
      }
    })

    await cab_detail.save();

    res.json({success: true, cab_detail: cab_detail});

  } catch(error){
    console.log(error)
    res.json({success: false, error_message: error.message});
  }
});

router.post('/update_cab', async function(req, res, next) {
  try{
    var req_body = req.body;
    
    var cab_detail = await Cab.findOne({_id: req_body.cab_id});
    if(cab_detail){
      cab_detail.cab_name = req_body.cab_name ? req_body.cab_name : cab_detail.cab_name;
      cab_detail.cab_type = req_body.cab_type ? req_body.cab_type : cab_detail.cab_type;
      cab_detail.location = (req_body.latitude && req_body.longitude) ? [Number(req_body.latitude), Number(req_body.longitude)] : cab_detail.location;
      cab_detail.driver_detail = {
        name: req_body.name ? req_body.name : cab_detail.driver_detail.name,
        phone: req_body.phone ? req_body.phone : cab_detail.driver_detail.phone,
        email: req_body.email ? req_body.email : cab_detail.driver_detail.email
      }
      cab_detail.vehicle_detail = {
        name: req_body.name ? req_body.name : cab_detail.vehicle_detail.name,
        type: req_body.type ? req_body.type : cab_detail.vehicle_detail.type,
        plate_no: req_body.plate_no ? req_body.plate_no : cab_detail.vehicle_detail.plate_no
      }

      await cab_detail.save();
      res.json({success: true, cab_detail: cab_detail});
    } else {
      res.json({success: false, error_message: 'cab not found'});
    }

  } catch(error){
    console.log(error)
    res.json({success: false, error_message: error.message});
  }
});

router.post('/get_cab_list', async function(req, res, next) {
  try{
    var req_body = req.body;
    
    var request_detail = await Request.findOne({_id: req_body.request_id});
    if(request_detail){
      var location_query = {
         $geoNear: {
            near: { type: "Point", coordinates: request_detail.pickup_location },
            distanceField: "distance",
            maxDistance: 2,
            spherical: true
         }
       }
      var cab_list = await Cab.aggregate([location_query])
      res.json({success: true, cab_list: cab_list});
    } else {
      res.json({success: false, error_message: 'request not found'});
    }

  } catch(error){
    console.log(error)
    res.json({success: false, error_message: error.message});
  }
});

module.exports = router;

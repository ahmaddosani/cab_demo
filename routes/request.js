var express = require('express');
var router = express.Router();
var Request = require('../models/request')

router.post('/create_request', async function(req, res, next) {
  try{
    var req_body = req.body;
    var request_detail = new Request({
      pickup_address: req_body.pickup_address,
      destination_address: req_body.destination_address,
      pickup_location: [Number(req_body.pickup_latitude), Number(req_body.pickup_longitude)],
      destination_location: [Number(req_body.destination_latitude), Number(req_body.destination_longitude)],
      user_detail: {
        name: req_body.name,
        phone: req_body.phone,
        email: req_body.email,
      }
    })
    await request_detail.save();

    res.json({success: true, request_detail: request_detail});
  } catch(error){
    console.log(error)
    res.json({success: false, error_message: error.message});
  }
});



module.exports = router;

const mongoose = require("mongoose");

const userDetailSchema = new mongoose.Schema({
  name: {type: String, default: ''},
  email: {type: String, default: ''},
  phone: {type: String, default: ''},
});

const request = new mongoose.Schema({
  pickup_address: {type: String, default: '', required: true},
  destination_address: {type: String, default: '', required: true},
  pickup_location: {
      type: [Number],
      index: '2dsphere', 
      required: true
  },
  destination_location: {
      type: [Number],
      index: '2dsphere', 
      required: true
  },
  user_detail: userDetailSchema
});

const Request = mongoose.model("request", request);

module.exports = Request;
const mongoose = require("mongoose");

const driverDetailSchema = new mongoose.Schema({
  name: {type: String, default: ''},
  email: {type: String, default: ''},
  phone: {type: String, default: '', required: true},
});

const vehicleDetailSchema = new mongoose.Schema({
  name: {type: String, default: ''},
  type: {type: String, default: ''},
  plate_no: {type: String, default: ''},
});

const cab = new mongoose.Schema({
  cab_name: {type: String, default: '', required: true},
  cab_type: {type: String, default: '', required: true},
  location: {
      type: [Number],
      index: '2dsphere', 
      required: true
  },
  driver_detail: driverDetailSchema,
  vehicle_detail: vehicleDetailSchema
});

const Cab = mongoose.model("cab", cab);

module.exports = Cab;